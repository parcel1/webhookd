#!/bin/bash

PROJECT=${project:-app}
BRANCH=${branch:-dev}
REGISTRY=${registry:-1}

TEMP_ALLOWED_PROJECTS=( $ALLOWED_PROJECTS )
TEMP_ALLOWED_BRANCHES=( $ALLOWED_BRANCHES )
GITLAB_REGISTRY_KEEP_NUM=${GITLAB_REGISTRY_KEEP_NUM:-5}

function try()
{
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function throw()
{
    exit $1
}

function catch()
{
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}

function throwErrors()
{
    set -e
}

function ignoreErrors()
{
    set +e
}

function elementExists() {
    local element=${1}            
    shift                                                           
    elements=($@)
    for i in "${elements[@]}" ; do
        if [ "$i" == "$element" ] ; then
            echo '1'
            return 0
        fi
    done
    echo '0'
    return 0
}

export NotAllowedBranch=102
export NotAllowedApplication=103

# start with a try
try
(   # open a subshell !!!
    IS_ALLOW=$(elementExists "$PROJECT" "${TEMP_ALLOWED_PROJECTS[@]}")
    [ $IS_ALLOW -eq 0 ] && throw $NotAllowedApplication    

    IS_ALLOW=$(elementExists "$BRANCH" "${TEMP_ALLOWED_BRANCHES[@]}")
    [ $IS_ALLOW -eq 0 ] && throw $NotAllowedBranch

    echo "Executing cleanup..."
    curl --request DELETE -sS \
      --data-urlencode "name_regex=^$BRANCH-[a-zA-Z0-9]{8}$" \
      --data "keep_n=${GITLAB_REGISTRY_KEEP_NUM}" \
      --header "PRIVATE-TOKEN: ${GITLAB_ACCESS_TOKEN}" \
      "https://gitlab.com/api/v4/projects/${PROJECT}/registry/repositories/${REGISTRY}/tags"

    echo "Cleanup done!"
)
# directly after closing the subshell you need to connect a group to the catch using ||
catch || {
    # now you can handle
    case $ex_code in
        $NotAllowedBranch)
            echo "Branch not allowed!"
        ;;        
        $NotAllowedApplication)
            echo "Application not allowed!"
        ;;
        *)
            echo "An unexpected exception was thrown"
            throw $ex_code # you can rethrow the "exception" causing the script to exit if not caught
        ;;
    esac
}