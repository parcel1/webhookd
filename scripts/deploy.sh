#!/bin/bash

APPLICATION=${application:-app}
BRANCH=${branch:-dev}
COMMIT=${commit:-default}
VERSION=${version:-v1.0}
APP_BRANCH=${app_branch:-develop}
COMMIT_MESSAGE=${commit_message:-updated}
DEPLOY_USER=${user:-anonymous}
WORKING_DIR=$APPLICATION-$BRANCH-$COMMIT

TEMP_ALLOWED_APPS=( $ALLOWED_APPS )
GIT_CONFIG_URI=$GIT_CONFIG_URI
VALUE_FILE=values-$VERSION.yaml

function try()
{
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function throw()
{
    exit $1
}

function catch()
{
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}

function throwErrors()
{
    set -e
}

function ignoreErrors()
{
    set +e
}

function elementExists() {
    local element=${1}            
    shift                                                           
    elements=($@)
    for i in "${elements[@]}" ; do
        if [ "$i" == "$element" ] ; then
            echo '1'
            return 0
        fi
    done
    echo '0'
    return 0
}

export CloneCodeFailure=100
export PushCodeFailure=101
export NotAllowedVersion=102
export NotAllowedApplication=103

# start with a try
try
(   # open a subshell !!!
    IS_ALLOW=$(elementExists "$APPLICATION" "${TEMP_ALLOWED_APPS[@]}")
    [ $IS_ALLOW -eq 0 ] && throw $NotAllowedApplication

    echo "Cloning config repository..."
    #export GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
    ssh -o StrictHostKeyChecking=no git@gitlab.hitdemo.top
    git clone -b $BRANCH --single-branch --depth 1 $GIT_CONFIG_URI /tmp/$WORKING_DIR
    [ $? != 0 ] && throw $CloneCodeFailure

    cd /tmp/$WORKING_DIR

    [ ! -f "helm/applications/$APPLICATION/$VALUE_FILE" ] && throw $NotAllowedVersion

    echo "Replacing..."

    sed -i "s/commitSha:.*/commitSha: $COMMIT/g" helm/applications/$APPLICATION/$VALUE_FILE
    APP_MESSAGE=$(echo $COMMIT_MESSAGE | sed "s/['/-/"/://"]*//g")
    echo $APP_MESSAGE
    sed -i "s/gitCommitMessage:.*/gitCommitMessage: \"$APP_MESSAGE\"/g" helm/applications/$APPLICATION/$VALUE_FILE
    
    echo $DEPLOY_USER
    sed -i "s/gitDeployUser:.*/gitDeployUser: \"$DEPLOY_USER\"/g" helm/applications/$APPLICATION/$VALUE_FILE
    echo $APP_BRANCH
    sed -i "s/\s*tag:.*/  tag: $APP_BRANCH/g" helm/applications/$APPLICATION/$VALUE_FILE
    echo "Replacing done!"
    echo "Commiting changes..."
    git add . 
    git commit -m "Update $APPLICATION to commit $COMMIT on $BRANCH environment"

    echo "Pushing changes..."
    git push
    [ $? != 0 ] && throw $PushCodeFailure

    echo "Patching done!"
)
# directly after closing the subshell you need to connect a group to the catch using ||
catch || {
    # now you can handle
    case $ex_code in
        $CloneCodeFailure)
            echo "Unable to clone repository!"
            rm -rf /tmp/$WORKING_DIR
        ;;
        $PushCodeFailure)
            echo "Cannot push changes to repository!"
        ;;
        $NotAllowedVersion)
            echo "Version not allowed!"
        ;;        
        $NotAllowedApplication)
            echo "Application not allowed!"
        ;;
        *)
            echo "An unexpected exception was thrown"
            rm -rf /tmp/$WORKING_DIR
            throw $ex_code # you can rethrow the "exception" causing the script to exit if not caught
        ;;
    esac
}

echo "Cleaning working dir"
rm -rf /tmp/$WORKING_DIR
