FROM ncarlier/webhookd
LABEL maintainer="anhtt@g-tech.ai"

WORKDIR /data
ENV APP_SCRIPTS_DIR=/data/scripts

RUN apk add --no-cache curl

COPY ./docker-entrypoint.sh /

# Add htpasswd
COPY scripts /data/scripts